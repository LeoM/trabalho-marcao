import React from 'react';
import { Spinner } from 'react-bootstrap';

import spinnerLoading from './../static/img/spinners.png';

var importSpinner = "import { Spinner } from 'react-bootstrap';";

class Componente5 extends React.Component {
  render() {
    return (
      <div className="container">
        <h3>Spinners de loading</h3>
        <h4>Variantes</h4>
        <p>
          Todas as variantes visuais padrão estão disponíveis para os dois estilos de animação, configurando a propriedade variant. Como alternativa, os spinners podem
          ser dimensionados de maneira personalizada com a propriedade style ou classes CSS personalizadas.
        </p>
        <div>
          <Spinner animation="border" variant="primary" />
          <Spinner animation="border" variant="secondary" />
          <Spinner animation="border" variant="success" />
          <Spinner animation="border" variant="danger" />
          <Spinner animation="border" variant="warning" />
          <Spinner animation="border" variant="info" />
          <Spinner animation="border" variant="light" />
          <Spinner animation="border" variant="dark" />
          <Spinner animation="grow" variant="primary" />
          <Spinner animation="grow" variant="secondary" />
          <Spinner animation="grow" variant="success" />
          <Spinner animation="grow" variant="danger" />
          <Spinner animation="grow" variant="warning" />
          <Spinner animation="grow" variant="info" />
          <Spinner animation="grow" variant="light" />
          <Spinner animation="grow" variant="dark" />
        </div>

        <br/>
        <p>O seguinte import é necessário para o funcionamento <strong>{importSpinner}</strong></p>
        <small>Código necessário:</small>
        <figure>
          <img alt="Spinners" src={spinnerLoading} style={{ width: '100%' }} />
          <figcaption><b>Spinners</b>.</figcaption>
        </figure>
        <br/>
        <h4>Algumas variações disponíveis</h4>
        <div class="overflow-auto mt-4 mb-5 border border-light"><table class="table bg-white mb-0 table-striped table-bordered"><thead><tr><th>Name</th><th>Type</th><th>Default</th><th>Description</th></tr></thead><tbody><tr class="prop-table-row"><td class="text-monospace">animation <span class="badge">required</span></td><td class="text-monospace"><div><span><code>'border'</code><span> | </span><code>'grow'</code></span></div></td><td><code class="PropTable-Code-module--cls1--2iQb5">true</code></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>Changes the animation style of the spinner.</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">as </td><td class="text-monospace"><div>elementType</div></td><td><code class="PropTable-Code-module--cls1--2iQb5">&lt;div&gt;</code></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>You can use a custom element type for this component.</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">children </td><td class="text-monospace"><div>element</div></td><td></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>This component may be used to wrap child elements or components.</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">role </td><td class="text-monospace"><div>string</div></td><td></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>An ARIA accessible role applied to the Menu component. This should generally be set to 'status'</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">size </td><td class="text-monospace"><div><span><code>'sm'</code></span></div></td><td></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>Component size variations.</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">variant </td><td class="text-monospace"><div><span><code>'primary'</code><span> | </span><code>'secondary'</code><span> | </span><code>'success'</code><span> | </span><code>'danger'</code><span> | </span><code>'warning'</code><span> | </span><code>'info'</code><span> | </span><code>'light'</code><span> | </span><code>'dark'</code></span></div></td><td></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>The visual color style of the spinner</p></div></td></tr><tr class="prop-table-row"><td class="text-monospace">bsPrefix </td><td class="text-monospace"><div>string</div></td><td><code class="PropTable-Code-module--cls1--2iQb5">'spinner'</code></td><td><div class="PropTable-PropDescription-module--cls1--29mNj"><p>Change the underlying component CSS base class name and modifier class names prefix. <strong>This is an escape hatch</strong> for working with heavily customized bootstrap css.</p></div></td></tr></tbody></table></div>
      </div>
    )
  }
}

export default Componente5;