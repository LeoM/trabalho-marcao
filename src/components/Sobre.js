import React from 'react';
import { Card } from 'react-bootstrap';

import muliterno from './../static/img/muliterno.png';
import tiohugo from './../static/img/tiohugo.jpg';
import espumoso from './../static/img/espumoso.png';

class Sobre extends React.Component {
  render() {
    return (
      <div className="container">
        <div style={{display: 'flex'}}>
          <Card style={{ width: '18rem', margin: '15px' }}>
            <Card.Img variant="top" src={muliterno} />
            <Card.Body>
              <Card.Title>Junior Maran</Card.Title>
              <Card.Text>
                <p style={{ fontWeight: '500', marginBottom: '0' }}>Desenvolvedor Junior</p>
                <span>21 anos</span>
              </Card.Text>
            </Card.Body>
          </Card>

          <Card style={{ width: '18rem', margin: '15px' }}>
            <Card.Img variant="top" src={tiohugo} />
            <Card.Body>
              <Card.Title>Guilherme Danielli Pereira</Card.Title>
              <Card.Text>
                <p style={{ fontWeight: '500', marginBottom: '0' }}>Desenvolvedor Junior</p>
                <span>21 anos</span>
              </Card.Text>
            </Card.Body>
          </Card>

          <Card style={{ width: '18rem', margin: '15px' }}>
            <Card.Img variant="top" src={espumoso} />
            <Card.Body>
              <Card.Title>Leonardo Marchese</Card.Title>
              <Card.Text>
                <p style={{ fontWeight: '500', marginBottom: '0' }}>Desenvolvedor Junior</p>
                <span>21 anos</span>
              </Card.Text>
            </Card.Body>
          </Card>
        </div>
      </div>
    )
  }
}

export default Sobre;