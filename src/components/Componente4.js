import React from 'react';
import { Breadcrumb } from 'react-bootstrap';
import breadcrumb from './../static/img/breadcrumb.png';

var importBreadcrumb = "import { Breadcrumb } from 'react-bootstrap';";

class Componente4 extends React.Component {
  render() {
    return (
      <div className="container">
        <h3>Breadcrumbs</h3>
        <p>Indique o local da página atual em uma hierarquia de navegação que adiciona automaticamente separadores via CSS. Adicione suporte ativo ao Breadcrumb.Item ativo. Não defina os atributos ativo e href juntos, o ativo substitui o elemento href e span é renderizado em vez de a.</p>

        <Breadcrumb>
          <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="https://getbootstrap.com/docs/4.0/components/breadcrumb/">
            Library
          </Breadcrumb.Item>
          <Breadcrumb.Item active>Data</Breadcrumb.Item>
        </Breadcrumb>

        <br/>
        <p>Import necessário para o funcionamento: <strong>{importBreadcrumb}</strong></p>
        <small>Código necessário</small>
        <figure>
          <img alt="Breadcrumb" src={breadcrumb} style={{ width: '100%' }} />
          <figcaption><b>Breadcrumb</b>.</figcaption>
        </figure>
      </div>
    )
  }
}

export default Componente4;