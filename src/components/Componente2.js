import React from 'react';
import { ButtonToolbar, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';
import tooltip from './../static/img/tooltip.png';

var direcoes = {
  'top': 'em cima',
  'right': 'na direita',
  'bottom': 'em baixo',
  'left': 'na esquerda'
}

var importsTooltip = "import { ButtonToolbar, Button, Tooltip, OverlayTrigger } from 'react-bootstrap';";

class Componente2 extends React.Component {
  render() {
    return (
      <div className="container">
        <h3>Tooltip</h3>
        <p>Tooltip é aquela moldura flutuante (pop up) que abre quando passa-se o mouse sobre um elemento da interface (normalmente uma palavra em um texto, link etc.) e que contém uma explicação adicional sobre tal elemento.</p>
        <ButtonToolbar>
          {['top', 'right', 'bottom', 'left'].map(placement => (
            <OverlayTrigger
              key={placement}
              placement={placement}
              overlay={
                <Tooltip id={`tooltip-${placement}`}>
                  Tooltip on <strong>{placement}</strong>.
                </Tooltip>
              }
            >
              <Button style={{margin: '5px'}} variant="secondary">Tooltip {direcoes[placement]}</Button>
            </OverlayTrigger>
          ))}
        </ButtonToolbar>

        <br/>
        <p>Para este exemplo, necessitamos do seguinte trecho de código: <strong>{importsTooltip}</strong> e o código abaixo</p>
        <small>Código necessário para o tooltip</small>
        <figure>
          <img alt="Tooltip" src={tooltip} style={{ width: '100%' }} />
          <figcaption><b>Tooltip</b>.</figcaption>
        </figure>

        <br/>
      </div>
    )
  }
}

export default Componente2;