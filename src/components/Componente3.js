import React from 'react';
import dropdownBasico from './../static/img/dropdown-basico.png';
import dropdown from './../static/img/dropdown.png';

import { Dropdown, DropdownButton } from 'react-bootstrap';

var importDropdown = "import { Dropdown, DropdownButton } from 'react-bootstrap';";

class Componente3 extends React.Component {
  render() {
    return (
      <div className="container">
        <h3>Dropdown</h3>
        <p>É necessário fazer o import dos componentes conforme o uso, nos casos a seguir: <strong>{importDropdown}</strong></p>
        <p>O menu suspenso b&aacute;sico &eacute; composto por um menu suspenso e um menu interno <strong>&lt;DropdownMenu&gt;</strong> e <strong>&lt;DropdownToggle&gt;</strong>. Por padr&atilde;o, o <strong>&lt;DropdownToggle&gt;</strong> renderiza um componente Button e aceita todos os mesmos objetos.</p>

        <Dropdown>
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            Botão Dropdown
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item>Ação</Dropdown.Item>
            <Dropdown.Item>Outra ação</Dropdown.Item>
            <Dropdown.Item>Outra</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        
        <br/>
        <figure>
          <img alt="Dropdown básico" src={dropdownBasico} style={{ width: '100%' }} />
          <figcaption><b>Código</b>. Dropdown básico</figcaption>
        </figure>

        <br />
        <p>Como o descrito acima &eacute; uma configura&ccedil;&atilde;o t&atilde;o comum, o react-bootstrap fornece o componente <strong>&lt;DropdownButton&gt;</strong> para ajudar a reduzir a digita&ccedil;&atilde;o. Forne&ccedil;a um suporte de t&iacute;tulo e alguns <strong>&lt;DropdownItem&gt;</strong> se voc&ecirc; est&aacute; pronto para come&ccedil;ar.</p>

        <DropdownButton id="dropdown-basic-button" title="Dropdown button">
          <Dropdown.Item>Ação</Dropdown.Item>
          <Dropdown.Item>Outra ação</Dropdown.Item>
          <Dropdown.Item>Outra</Dropdown.Item>
        </DropdownButton>

        <br/>
        <figure>
          <img alt="Dropdown" src={dropdown} style={{ width: '100%' }} />
          <figcaption><b>Código</b>. Dropdown</figcaption>
        </figure>
      </div>
    )
  }
}

export default Componente3;