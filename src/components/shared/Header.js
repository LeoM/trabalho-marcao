import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';

import { Link } from 'react-router-dom';

class Header extends React.Component {
  render() {
    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>Navbar</Navbar.Brand>
        <Nav className="mr-auto">
          <Link className="nav-link" to="/">Home</Link>
          <Link className="nav-link" to="/componente1">Componente 1</Link>
          <Link className="nav-link" to="/componente2">Componente 2</Link>
          <Link className="nav-link" to="/componente3">Componente 3</Link>
          <Link className="nav-link" to="/componente4">Componente 4</Link>
          <Link className="nav-link" to="/componente5">Componente 5</Link>
          <Link className="nav-link" to="/sobre">Sobre</Link>
        </Nav>
      </Navbar>
    )
  }
}

export default Header;