import React from 'react';

class Footer extends React.Component {
  render() {
    return (
      <div className='footer'>
        <a href="https://react-bootstrap.netlify.com" target="blank">React Bootstrap</a>
      </div>
    )
  }
}

export default Footer;