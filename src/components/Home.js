import React from 'react';
import versaoNode from './../static/img/versao-node.png';
import aplicacaoInicial from './../static/img/aplicacao-inicial.png';

var example = '{ Button }';

class Home extends React.Component {
  render() {
    return (
      <div className="container">
        <hr/>
        <h3>Configurando o Ambiente</h3>

        <div>
          <p>
            <strong>React é uma biblioteca/framework JavaScript</strong> para criar aplicações que rodam no navegador, porém os pacotes que iremos utilizar durante o desenvolvimento serão gerenciados pelo Node Package Manager (NPM), inclusive o próprio React.
          </p>
          <p>Começaremos então indo até o site oficial do <a title="Site do Node.js" href="https://nodejs.org/en/" target="blank">Node.js</a> e iremos instalar sua versão LTS, como ilustra a <strong>Figura 1.</strong></p>

          <figure> 
            <img alt="Site do Node.js" src={versaoNode}  style={{width: '100%'}}/>
            <figcaption><b>Figura 1</b>. Site do Node.js</figcaption>
          </figure>

          <p>Quando instalamos o Node.js o NPM também é instalado no computador como parte do mesmo pacote, para utilizá-lo basta abrir a ferramenta de linha de comando do seu sistema operacional, por exemplo o cmd do Windows ou o terminal do Linux, e digitar npm, uma lista com os comandos será exibida.</p>

          <hr/>
          <h3>Criando o Projeto</h3>

          <p>Após a instalação do Node.js, precisamos instalar a ferramenta de linha de comando <strong>create-react-app</strong> para iniciarmos nosso projeto</p>
          <p> Para instalar o create-react-app digite o comando <strong>npm install -g create-react-app</strong> em seu terminal e aguarde a finalização.</p>

          <p> 
            Quando a instalação terminar, o comando create-react-app vai estar disponível em seu terminal e a partir dele podemos criar o projeto da nossa aplicação. Vá então até uma pasta adequada do seu computador pelo terminal e digite o comando:
            <br/><br/><strong>create-react-app [pasta do projeto]</strong>
          </p>

          <p> O processo de criação do projeto pode levar alguns instantes enquanto <strong>create-react-app</strong> monta todo o boilerplate mínimo que uma aplicação React para a web precisa ter. </p>

          <p>Após o término da criação do projeto, devemos acessar a pasta criada através do comando <strong>cd [nome do projeto]</strong> e em seguida iniciar o servidor com <strong>npm start</strong>.</p>

          <p>Em seguida, uma página web será aberta automaticamente: </p>

          <figure> 
            <img alt="Aplicação Inicial ReactJs" src={aplicacaoInicial} style={{width: '100%'}}/>
            <figcaption><b>Figura 2</b>. Aplicação Inicial</figcaption>
          </figure>

          <h5>Pronto! Estamos com o ambiente pronto para iniciarmos o desenvolvimento.</h5>

          <hr/>

          <h3>Instalando React Bootstrap</h3>

          <p>Para instalarmos o <strong>React Bootstrap</strong> devemos rodar o comando <strong>npm install react-bootstrap bootstrap</strong> no terminal.</p>

          <p>E em seguida adicionarmos o trecho <strong>import 'bootstrap/dist/css/bootstrap.min.css';</strong> no arquivo <strong>src/index.js ou src/App.js</strong></p>

          <h5>Importando</h5>
          <p>Você deve importar os compomentes individualmente de acordo com a necessidade de uso, por exemplo: <strong>import { example } from 'react-bootstrap';</strong></p>

          <hr/>
          <h5>A partir disto, já conseguimos fazer uso do <strong>React Bootstrap</strong> em nosso projeto!</h5>
        </div>
      </div>
    )
  }
}

export default Home;