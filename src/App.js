import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { HashRouter, Switch, Route } from 'react-router-dom';
import Header from './components/shared/Header';
import Footer from './components/shared/Footer';
import Home from './components/Home';
import Sobre from './components/Sobre';
import Componente1 from './components/Componente1';
import Componente2 from './components/Componente2';
import Componente3 from './components/Componente3';
import Componente4 from './components/Componente4';
import Componente5 from './components/Componente5';

function App() {
  return (
    <HashRouter>
      <Header />
      <Switch>
        <Route path="/" exact={true} component={Home} />
        <Route path="/componente1" component={Componente1} />
        <Route path="/componente2" component={Componente2} />
        <Route path="/componente3" component={Componente3} />
        <Route path="/componente4" component={Componente4} />
        <Route path="/componente5" component={Componente5} />
        <Route path="/sobre" component={Sobre} />
      </Switch>
      <Footer />
    </HashRouter>
  );
}

export default App;
